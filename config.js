/* 
    Act as database for storing specific configuration for each Student Year.
*/
const config = {
    Year_1: {
        name: "Year 1",
        numRange: {
            min: 0,
            max: 100
        },
        numOperator: 1,
        opArr: ["+", "-"]
    },
    Year_2: {
        name: "Year 2",
        numRange: {
            min: 0,
            max: 1000
        }, 
        numOperator: 1,
        opArr: ["+", "-", "/", "*"]
    }
}

/*
    Provide mapping for the id in html page.
*/
const dispMapping = {
    button: {
        number: [0,1,2,3,4,5,6,7,8,9],
        util: {
            clear: "clear",
            empty: "temp"
        }
    },
    display: {
        displayNumber: "disp1",
        displayOperator: "disp2",
    },
    div: {
        chooseYear: "div1",
        theCalculator: "div2"
    }
}