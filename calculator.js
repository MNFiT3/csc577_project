//Object for calculator
function Calculator(yearConfig) {
    this.id = yearConfig.id;
    this.minNum = yearConfig.numRange.min;
    this.maxNum = yearConfig.numRange.max;
    this.numOp = yearConfig.numOperator;
    this.expression = "";
    this.operator = "";
    
    this.addOperator = function(op){
        this.operator += op;
    }
    
    this.sizeOfOperator = function(){
        return this.operator.length;
    }
        
    this.addExpression = function(expr) {
        this.expression += expr;
    }
    
    this.calculate = function(callBackFunction) {
        var result = eval(this.expression);
    
        if(result >= this.minNum && result <= this.maxNum){
            var temp = result + " ";
            var indexOfTemp = temp.indexOf(".");
            if(indexOfTemp != -1){
                result = parseFloat(temp.substr(0, (indexOfTemp + 3)));
                var dtf = new decimalToFraction(result);
                var a = (dtf.top / dtf.bottom) + "";
                var arr;
                
                if(a.indexOf(".") != -1){
                    arr = a.split(".");
                    result = arr[0] + " " + (parseFloat("0." + arr[1]) * dtf.bottom) + "/" + dtf.bottom;
                    if(parseInt(arr[0]) == 0){
                       result = dtf.display;
                    }
                }else{
                    result = dtf.display;
                }
            }
            return result;
        }else{
            alert("Result is out of range");
            callBackFunction();
            return "&nbsp;";
        }
    
    }
    
    this.reset = function(){
        this.expression = "";
        this.number = "";
        this.operator = "";
    }
}