var number = "";
var dispOP = "";
var tempNum = "";
var app;

//load years in config file
var dispBtn = "";
Object.keys(config).forEach(function(obj){
    dispBtn += `    
    <div class='col'>
        <button type='button' onclick='setConfig(config.`+obj+`)' class='btn btn-success'>`+ config[obj].name +`</button>
    </div>
    `;
})
$("#btnYear").html(dispBtn);


function setConfig(conf){
    app = new Calculator(conf);
    loadUi(conf.opArr);
    $("#chooseYear").css("display", "none");
    $("#theCalculator").css("display", "block");
    $("#theBody").css("background-image", "url(assets/img/background-calculator.png)");
}

function loadUi(obj){
    obj.forEach(function(o){
        switch(o){
            case "*": $("#op0").html("x"); break;
            case "/": $("#op1").html("÷"); break;
            case "-": $("#op2").html("-"); break;
            case "+": $("#op3").html("+"); break;
            default:
        }
    })
}


$(document).ready(function () {
    var soundId = "click" + Math.floor(Math.random() * 3);
    
    //Handles the Ui input
    $("#btn_").click(function(o){
        document.getElementById(soundId).play();
        var data = o.target.innerHTML;
        
        //Number
        for(i = 0; i < dispMapping.button.number.length; i++){
            if(dispMapping.button.number[i] == data){
                update(data);
            }
        }
        
        //Operator and Utils
        switch(data){
            case "Clear": clear(); break;
            case "=": calculate(); break;
            case "÷": 
            case "x":
            case "-": 
            case "+": displayOperator(data); break;
        }
    })

    //Update and store expression for UI
    function update(num) {
        number += num;
        tempNum += num;
        if(!(eval(tempNum) <= app.maxNum)){
            if(num != ""){
               alert("Maximun number is " + app.maxNum + " only");
                clear();
            }
        }else{
            app.addExpression(num);
            $("#disp_number").html(number);
        }
        
    }

    //Reset all variable to default
    function clear() {
        $("#disp_operator").html("&nbsp;");
        $("#disp_number").html("&nbsp;");
        $("#disp_result").html("&nbsp;");
        number = "";
        dispOP = "";
        tempNum = "";
        app.reset();
    }

    //Calculate the expression that from Calculator Object
    function calculate() {
        try {
            const resultValue = app.calculate(clear);
            $("#disp_result").html(resultValue);
            
        } catch (error) {
            alert("Expression error. Please try again");
            clear();
        }
    }

    //Utils to handle operator UI
    function displayNumber(num) {
        $("#disp_operator").html(number);
    }

    //Utils to handle operator UI and update the expression entered to Calculator object
    function displayOperator(op) {
        if ((app.sizeOfOperator() < app.numOp) && (tempNum != 0)) {
            if (op == "÷") {
                app.addExpression("/");
            } else if (op == "x") {
                app.addExpression("*");
            } else {
                app.addExpression(op);
            }
            var temp = $("#disp_number").html();
            number += "<br />";
            dispOP += op + "<br />";
            tempNum = "";
            app.addOperator(op);
            var temp = $("#disp_operator").html();
            $("#disp_operator").html(dispOP);
        }
    }
});