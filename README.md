# Primary School Calculator : Year 1 and 2 Calculation System


The system name is “Primary School Calculator (PSC)”. It is used by first year and second year primary school students to make calculations. The calculation for first year student only involved addition and subtraction with the number range from 0 to 100 and for the second-year student, it has two extra operation which is multiplication and division with the number range limited to 1000. 

    • Accept the expression.
    • Check expression to verify it is within the intended parameters. 
    • Calculate the expression.
    • Display the result. 

# Team Members
    • Muhammad Eirfan bin Azmi (2017946213)
    • Muhammad Nurfitri bin Muhd Sazali (2017193763)
    • Mohamad Izzulamir bin Shahir (2017578891)
    • Nurul Aliya Tasya binti Baharuddin (2017354785)
    
# Prepared For
    • Mr. Wan Ya bin Wan Hussin 
 